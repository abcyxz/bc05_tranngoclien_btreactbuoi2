import React, { Component } from 'react'

import { dataGlasses } from './dataGlasses';
import Details from './Details';

export default class Layout1 extends Component {
state ={ 
    glasses:dataGlasses,
    detail: {},
}
handleDetails = (detailOfGlasses) => {
this.setState({
    detail: detailOfGlasses

})


}

  render() {
    return (
        <div className='container'>
       <Details detail={this.state.detail} />
        <div className="layout1 row py-5">
          {this.state.glasses.map((item, index) => {
            return (
            <div className="card text-left col-2">
  <img 
  onClick={() => this.handleDetails(item)
}
  className="card-img-top"
  style={{width:"150px" }}
  src={item.url} alt="" />
 
</div>

            );
          })}
        </div>
        
      </div>
    )
  }
}
