import React, { Component } from 'react'

export default class Details extends Component {
  render() {
    return (
      <div className='glassesafter'>
        <img src={this.props.detail.url} alt="" />
        <div className='glassesinfo'>
          <h1>{this.props.detail.name}</h1>
          <p1>{this.props.detail.price}</p1>
          <p>{this.props.detail.desc}</p>
        </div>
      </div>
    )
  }
}
